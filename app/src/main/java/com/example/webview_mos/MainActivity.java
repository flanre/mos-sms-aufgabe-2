package com.example.webview_mos;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    WebView webview = new WebView(this);
    webview.setWebViewClient(new WebViewClient());
    setContentView(webview);
    WebSettings settings=webview.getSettings();
    settings.setJavaScriptEnabled(true);
    settings.setAllowFileAccessFromFileURLs(true);
    settings.setAllowUniversalAccessFromFileURLs(true);
    webview.loadUrl("http://google.de");
  }
}
